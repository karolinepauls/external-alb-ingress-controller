"""Test setup."""
import pytest  # type: ignore
from typing import Dict, NamedTuple, Tuple, Iterator
from pathlib import Path
from time import sleep
import boto3  # type: ignore
from kubernetes import client, config  # type: ignore
from kubernetes.utils import create_from_yaml  # type: ignore
from moto import mock_elbv2, mock_ec2  # type: ignore
from external_alb.datamodel import ALBInfo, ListenerRule, ALBTarget, RuleTargetMap


LB_NAME = 'test-elb'
TESTS_DIR = Path(__file__).parent


class EC2Resources(NamedTuple):
    """EC2 resources ALB depends on."""

    vpc_id: str
    security_group_id: str
    subnet_ids: Tuple[str, ...]


@pytest.fixture
def kube_api():
    """Connect to Kubernetes."""
    config.load_kube_config()
    api = client.CoreV1Api()
    return api


@pytest.fixture
def kube_ev1b1_api(kube_api):
    """Get the extensions/v1beta1 API."""
    return client.ExtensionsV1beta1Api(client.ApiClient())


@pytest.fixture
def clean_kube(kube_api, kube_ev1b1_api) -> Iterator[None]:
    """Clean up Kube resources on teardown."""
    def get_resource(api_call):
        return {(i.metadata.name, i.metadata.namespace) for i in api_call().items}

    def get_resources():
        pods = get_resource(kube_api.list_pod_for_all_namespaces)
        services = get_resource(kube_api.list_service_for_all_namespaces)
        ingresses = get_resource(kube_ev1b1_api.list_ingress_for_all_namespaces)
        namespaces = {i.metadata.name for i in kube_api.list_namespace().items}
        return pods, services, ingresses, namespaces

    resources_before = get_resources()

    yield

    resources_after_test = get_resources()
    created_pods, created_services, created_ingresses, created_namespaces = [
        after - before for before, after in zip(resources_before, resources_after_test)
    ]
    for pod_name, pod_namespace in created_pods:
        kube_api.delete_namespaced_pod(pod_name, pod_namespace, grace_period_seconds=0)
    for service_name, service_namespace in created_services:
        kube_api.delete_namespaced_service(service_name, service_namespace, grace_period_seconds=0)
    for ingress_name, ingress_namespace in created_ingresses:
        kube_ev1b1_api.delete_namespaced_ingress(ingress_name, ingress_namespace,
                                                 grace_period_seconds=0)
    for namespace in created_namespaces:
        kube_api.delete_namespace(namespace, grace_period_seconds=0)

    while True:
        resources_after_deletion = get_resources()
        if resources_after_deletion != resources_before:
            sleep(0.1)
        else:
            break


@pytest.fixture
def kube_resources(kube_api, clean_kube) -> Dict[str, str]:
    """Create resources in the cluster as in the "before" sample."""
    create_from_yaml(kube_api.api_client, TESTS_DIR / 'sample.yaml')
    # Wait until all pods have IPs.
    while True:
        pod_to_ip = {pod.metadata.name: str(pod.status.pod_ip)
                     for pod in kube_api.list_pod_for_all_namespaces().items}
        if not any(ip == 'None' for ip in pod_to_ip.values()):
            break
    return pod_to_ip


@pytest.fixture
def aws_mock():
    """Mock ALB and EC2 with Moto."""
    mock_elbv2().start()
    mock_ec2().start()


@pytest.fixture
def ec2(aws_mock):
    """Get a mocked EC2 resource."""
    return boto3.resource("ec2", region_name="us-east-1")


@pytest.fixture
def elbv2(aws_mock):
    """Get a mocked ALB (elbv2) client."""
    return boto3.client("elbv2", region_name="us-east-1")


@pytest.fixture
def ec2_resources(ec2) -> EC2Resources:
    """Create EC2 resources needed for the ALB."""
    security_group = ec2.create_security_group(
        GroupName="a-security-group", Description="A security group"
    )
    vpc = ec2.create_vpc(CidrBlock="172.28.7.0/24", InstanceTenancy="default")
    subnet1 = ec2.create_subnet(
        VpcId=vpc.id, CidrBlock="172.28.7.192/26", AvailabilityZone="us-east-1a"
    )
    subnet2 = ec2.create_subnet(
        VpcId=vpc.id, CidrBlock="172.28.7.0/26", AvailabilityZone="us-east-1b"
    )
    return EC2Resources(vpc_id=vpc.id, security_group_id=security_group.id,
                        subnet_ids=(subnet1.id, subnet2.id))


@pytest.fixture
def alb(ec2_resources: EC2Resources, elbv2) -> ALBInfo:
    """Create a fake ALB in Moto, without any targets."""
    load_balancer_resp = elbv2.create_load_balancer(
        Name=LB_NAME,
        Subnets=ec2_resources.subnet_ids,
        SecurityGroups=[ec2_resources.security_group_id],
        Scheme="internal",
        Tags=[{"Key": "key_name", "Value": "a_value"}],
    )
    load_balancer_arn = load_balancer_resp['LoadBalancers'][0]['LoadBalancerArn']

    # Redirect to HTTPS
    elbv2.create_listener(
        LoadBalancerArn=load_balancer_arn,
        Protocol="HTTP",
        Port=80,
        DefaultActions=[
            {
                "Type": "redirect",
                "RedirectConfig": {
                    "Protocol": "HTTPS",
                    "Port": "443",
                    "Host": "#{host}",
                    "Path": "#{path}",
                    "Query": "#{query}",
                    "StatusCode": "302",
                }
            }
        ],
    )
    listener_resp = elbv2.create_listener(
        LoadBalancerArn=load_balancer_arn,
        Protocol="HTTPS",
        Port=443,
        DefaultActions=[
            {
                "Type": "fixed-response",
                "FixedResponseConfig": {
                    "StatusCode": "404",
                    "MessageBody": "Not found",
                    "ContentType": "text/plain",
                }
            }
        ],
    )
    listener_arn = listener_resp['Listeners'][0]['ListenerArn']
    return ALBInfo(name=LB_NAME, vpc_id=ec2_resources.vpc_id, listener_arn=listener_arn,
                   max_priority=0)


@pytest.fixture
def alb_resources(elbv2, alb: ALBInfo, kube_resources: Dict[str, str]) -> ALBInfo:
    """
    Create resources in the cluster as in the "before" sample.

    No teardown.
    """
    pod_ips = kube_resources

    def make_tg(service_name: str):
        [target_group] = elbv2.create_target_group(
            Name=service_name,  # Mirroring Service name
            Protocol="HTTP",
            # Targets specify their own ports,
            Port=1,
            VpcId=alb.vpc_id,
            HealthCheckProtocol="HTTP",
            # Should be possible to specify healthchecks using an annotation.
            HealthCheckPort="8000",
            HealthCheckPath="/",
            HealthCheckIntervalSeconds=5,
            HealthCheckTimeoutSeconds=5,
            HealthyThresholdCount=5,
            UnhealthyThresholdCount=2,
            Matcher={"HttpCode": "200"},
        )["TargetGroups"]
        return target_group

    target_group_some_svc_a = make_tg("some-svc-a")
    target_group_some_svc_b = make_tg("some-svc-b")
    target_group_endpoint_svc_path_1 = make_tg("endpoint-svc-path-1")
    target_group_endpoint_svc_path_2 = make_tg("endpoint-svc-path-2")
    target_group_endpoint_svc_just_path = make_tg("endpoint-svc-just-path")

    elbv2.register_targets(
        TargetGroupArn=target_group_some_svc_a["TargetGroupArn"],
        Targets=[
            {"Id": pod_ips['some-pod-a-1'], "Port": 5000},
            {"Id": pod_ips['some-pod-a-2'], "Port": 6000},
        ],
    )
    elbv2.register_targets(
        TargetGroupArn=target_group_some_svc_b["TargetGroupArn"],
        Targets=[
            {"Id": pod_ips['some-pod-b-1'], "Port": 8000},
            {"Id": pod_ips['some-pod-b-2'], "Port": 8000},
        ],
    )
    elbv2.register_targets(
        TargetGroupArn=target_group_endpoint_svc_path_1["TargetGroupArn"],
        Targets=[
            {"Id": pod_ips['endpoint-pod-path-1'], "Port": 8000},
        ],
    )
    elbv2.register_targets(
        TargetGroupArn=target_group_endpoint_svc_path_2["TargetGroupArn"],
        Targets=[
            {"Id": pod_ips['endpoint-pod-path-2'], "Port": 8000},
        ],
    )
    elbv2.register_targets(
        TargetGroupArn=target_group_endpoint_svc_just_path["TargetGroupArn"],
        Targets=[
            {"Id": pod_ips['endpoint-pod-just-path-1'], "Port": 8000},
        ],
    )

    elbv2.create_rule(
        ListenerArn=alb.listener_arn,
        Priority=1,
        Conditions=[
            {"Field": "host-header", "Values": ['endpoint-a-1.cluster1.domain.example']},
        ],
        Actions=[
            {"TargetGroupArn": target_group_some_svc_a["TargetGroupArn"], "Type": "forward"}
        ],
    )
    elbv2.create_rule(
        ListenerArn=alb.listener_arn,
        Priority=2,
        Conditions=[
            {"Field": "host-header", "Values": ['endpoint-a-2.cluster1.domain.example']},
        ],
        Actions=[
            {"TargetGroupArn": target_group_some_svc_b["TargetGroupArn"], "Type": "forward"}
        ],
    )
    elbv2.create_rule(
        ListenerArn=alb.listener_arn,
        Priority=3,
        Conditions=[
            {"Field": "host-header", "Values": ['endpoint-with-paths.cluster1.domain.example']},
            {"Field": "path-pattern", "Values": ['/path-a']},
        ],
        Actions=[
            {"TargetGroupArn": target_group_endpoint_svc_path_1["TargetGroupArn"],
             "Type": "forward"}
        ],
    )
    elbv2.create_rule(
        ListenerArn=alb.listener_arn,
        Priority=4,
        Conditions=[
            {"Field": "host-header", "Values": ['endpoint-with-paths.cluster1.domain.example']},
            {"Field": "path-pattern", "Values": ['/path-b']},
        ],
        Actions=[
            {"TargetGroupArn": target_group_endpoint_svc_path_2["TargetGroupArn"],
             "Type": "forward"}
        ],
    )
    elbv2.create_rule(
        ListenerArn=alb.listener_arn,
        Priority=5,
        Conditions=[
            {"Field": "path-pattern", "Values": ['/just-path']},
        ],
        Actions=[
            {"TargetGroupArn": target_group_endpoint_svc_just_path["TargetGroupArn"],
             "Type": "forward"}
        ],
    )
    return alb._replace(max_priority=5)


@pytest.fixture
def sample_actual(kube_resources: Dict[str, str]) -> RuleTargetMap:
    """Sample of actual routing (compatible with ``alb_resources``)."""
    pod_ips = kube_resources
    actual = {
        ListenerRule(host='endpoint-a-1.cluster1.domain.example', path=None): frozenset({
            ALBTarget(ip=pod_ips['some-pod-a-1'], http_port=5000),
            ALBTarget(ip=pod_ips['some-pod-a-2'], http_port=6000),
        }),
        ListenerRule(host='endpoint-a-2.cluster1.domain.example', path=None): frozenset({
            ALBTarget(ip=pod_ips['some-pod-b-1'], http_port=8000),
            ALBTarget(ip=pod_ips['some-pod-b-2'], http_port=8000),
        }),
        ListenerRule(host='endpoint-with-paths.cluster1.domain.example', path='/path-a'):
            frozenset({ALBTarget(ip=pod_ips['endpoint-pod-path-1'], http_port=8000)}),
        ListenerRule(host='endpoint-with-paths.cluster1.domain.example', path='/path-b'):
            frozenset({ALBTarget(ip=pod_ips['endpoint-pod-path-2'], http_port=8000)}),
        ListenerRule(host=None, path='/just-path'): frozenset({
            ALBTarget(ip=pod_ips['endpoint-pod-just-path-1'], http_port=8000),
        }),
    }
    return actual


@pytest.fixture
def sample_desired(kube_resources: Dict[str, str]) -> RuleTargetMap:
    """Sample of desired routing (compatible with the results of ``kube_resources``)."""
    pod_ips = kube_resources
    desired: RuleTargetMap = {
        ListenerRule(host='endpoint-a-2.cluster1.domain.example', path=None): frozenset({
            ALBTarget(ip=pod_ips['some-pod-b-1'], http_port=8001),
            ALBTarget(ip=pod_ips['some-pod-b-2'], http_port=8000)}),
        ListenerRule(host='endpoint-with-paths.cluster1.domain.example', path='/path-a'):
            frozenset(),
        ListenerRule(host='endpoint-with-paths.cluster1.domain.example', path='/path-b'):
            frozenset({ALBTarget(ip=pod_ips['endpoint-pod-path-2'], http_port=8000)}),
        ListenerRule(host=None, path='/just-path'): frozenset({
            ALBTarget(ip=pod_ips['endpoint-pod-just-path-1'], http_port=8000),
            ALBTarget(ip='10.244.0.110', http_port=8000)}),
        ListenerRule(host='new-endpoint.cluster1.domain.example', path='/path-b'): frozenset({
            ALBTarget(ip='18.222.0.330', http_port=8001)}),
    }
    return desired
