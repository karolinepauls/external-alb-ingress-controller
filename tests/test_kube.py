"""Test scanning Kubernetes LB routing setup."""
from typing import Dict, Union, Iterable, Any, Optional
from typing_extensions import TypedDict
import pytest  # type: ignore
from kubernetes.client.models.v1_pod import V1Pod  # type: ignore
from external_alb.datamodel import ALBTarget
from external_alb.kube import (
    query_service, get_pod_destination, resolve_service,
    KubeService,
    MissingService, ServiceMissingMappedPort,
)
from tests.wait import wait_for_ip


class ServicePort(TypedDict):
    """Kubernetes Service.spec.ports item."""

    protocol: str
    port: int
    targetPort: Union[str, int]


PORT_NAMED: ServicePort = {
  'protocol': 'TCP',
  'port': 8000,
  'targetPort': 'http',
}
PORT_UNNAMED: ServicePort = {
  'protocol': 'TCP',
  'port': 5000,
  'targetPort': 5555,
}
PORT_UDP: ServicePort = {
  'protocol': 'UDP',
  'port': 5000,
  'targetPort': 5555,
}

POD_LABELS = {
    'app.kubernetes.io/name': 'a-name',
    'app.kubernetes.io/instance': 'an-instance',
}

TCP_CONTANER_PORT = 5000


def make_kube_pod(
    kube_api,
    namespace='default',
    name='abc',
    labels=None,
    node_selector=None,
    wait=True,
    port=TCP_CONTANER_PORT,
    ports: Optional[Iterable[Dict[str, Any]]] = None,
) -> V1Pod:
    """Create a testing pod."""
    if labels is None:
        labels = {
            'app.kubernetes.io/name': 'some-pod',
            'app.kubernetes.io/instance': 'some-pod-a',
        }

    if ports is None:
        ports = [{
            'name': 'http',
            'containerPort': port,
            'protocol': 'TCP',
        }]

    pod = kube_api.create_namespaced_pod(
        namespace,
        body={
            'metadata': {
                'namespace': namespace,
                'name': name,
                'labels': labels,
            },
            'spec': {
                'containers': [
                    {
                        'name': 'abc',
                        'image': 'k8s.gcr.io/pause',
                        'ports': ports,
                    }
                ],
                'nodeSelector': node_selector
            }
        }
    )
    if wait:
        pod = wait_for_ip(kube_api, pod)
    return pod


def make_kube_service(
    kube_api,
    namespace='default',
    name='abc',
    selector: Dict[str, str] = {},
    ports: Iterable[ServicePort] = (),
):
    """
    Create a testing service.

    Ports are Kubernetes service ports.
    """
    service = kube_api.create_namespaced_service(
        namespace,
        body={
            'metadata': {
                'namespace': namespace,
                'name': name,
            },
            'spec': {
                'selector': selector,
                'ports': list(ports),
            }
        }
    )
    return service


def test_query_service(kube_api, clean_kube) -> None:
    """Test querying Kubernetes services."""
    with pytest.raises(MissingService):
        query_service(kube_api, namespace='default', service_name='missing', service_port=20)

    service_no_ports_match = make_kube_service(kube_api, 'default', 'no-ports-match',
                                               ports=[PORT_UDP])

    with pytest.raises(ServiceMissingMappedPort):
        query_service(kube_api, namespace='default',
                      service_name=service_no_ports_match.metadata.name,
                      service_port=20)

    # Cannot be found if looking in another namespace.
    with pytest.raises(MissingService):
        query_service(kube_api, namespace='another',
                      service_name=service_no_ports_match.metadata.name,
                      service_port=20)

    # Named TCP target port search.
    service_named_tcp = make_kube_service(kube_api, 'default', 'named-tcp', ports=[PORT_NAMED])
    named_tcp_result = query_service(kube_api, namespace='default',
                                     service_name=service_named_tcp.metadata.name,
                                     service_port=PORT_NAMED['port'])
    assert named_tcp_result == KubeService(pod_selector={}, http_port=PORT_NAMED['targetPort'])
    # Looking for a wrong port yields nothing.
    with pytest.raises(ServiceMissingMappedPort):
        query_service(kube_api, namespace='default',
                      service_name=service_named_tcp.metadata.name,
                      service_port=121)

    # Numeric TCP port search.
    service_unnamed_tcp = make_kube_service(kube_api, 'default', 'unnamed-tcp',
                                            ports=[PORT_UNNAMED])
    unnamed_tcp_result = query_service(kube_api, namespace='default',
                                       service_name=service_unnamed_tcp.metadata.name,
                                       service_port=PORT_UNNAMED['port'])
    assert unnamed_tcp_result == KubeService(pod_selector={},
                                             http_port=PORT_UNNAMED['targetPort'])

    # Successful search in another namespace.
    kube_api.create_namespace(body={'metadata': {'name': 'another'}})
    service_another_ns = make_kube_service(kube_api, 'another', 'named-tcp', ports=[PORT_NAMED])
    another_ns_result = query_service(kube_api, namespace='another',
                                      service_name=service_another_ns.metadata.name,
                                      service_port=PORT_NAMED['port'])
    assert another_ns_result == KubeService(pod_selector={},
                                            http_port=PORT_NAMED['targetPort'])

    # UDP ports discarded.
    service_udp = make_kube_service(kube_api, 'default', 'udp', ports=[PORT_UDP])
    with pytest.raises(ServiceMissingMappedPort):
        query_service(kube_api, namespace='default',
                      service_name=service_udp.metadata.name,
                      service_port=PORT_NAMED['port'])

    # Pod selector returned.
    selector = {
        'name': '121',
        'ddd': 'fff',
    }
    service_pod_selector = make_kube_service(kube_api, 'default', 'pod-selector',
                                             selector=selector, ports=[PORT_NAMED])
    pod_selector_result = query_service(kube_api, namespace='default',
                                        service_name=service_pod_selector.metadata.name,
                                        service_port=PORT_NAMED['port'])
    assert pod_selector_result == KubeService(pod_selector=selector,
                                              http_port=PORT_NAMED['targetPort'])


def test_get_pod_destination(kube_api, clean_kube) -> None:
    """Test building ALBTargets from pods."""
    # Can't immediately use the namespace so create it here.
    kube_api.create_namespace(body={'metadata': {'name': 'another'}})

    assert get_pod_destination(kube_api, 'default', {'no': 'thing'}, 1111) == set()

    pod = make_kube_pod(kube_api)
    labels = pod.metadata.labels
    port = pod.spec.containers[0].ports[0].container_port
    ip = pod.status.pod_ip

    targets = get_pod_destination(kube_api, 'default', labels, port)
    assert targets == {ALBTarget(ip=ip, http_port=port)}

    targets_by_name = get_pod_destination(kube_api, 'default', labels, 'http')
    assert targets_by_name == targets

    bad_port_targets = get_pod_destination(kube_api, 'default', labels, 'sftp')
    assert bad_port_targets == set()

    # Another pod, matching the same labels.
    same_labels_pod = make_kube_pod(kube_api, name='same-ns')
    same_labels_labels = same_labels_pod.metadata.labels
    same_labels_port = same_labels_pod.spec.containers[0].ports[0].container_port
    same_labels_ip = same_labels_pod.status.pod_ip
    same_labels_targets = get_pod_destination(kube_api, 'default', same_labels_labels,
                                              same_labels_port)
    previous_pod_targets = get_pod_destination(kube_api, 'default', labels, port)
    assert same_labels_targets == previous_pod_targets == {
        ALBTarget(ip=ip, http_port=port),
        ALBTarget(ip=same_labels_ip, http_port=same_labels_port),
    }

    # Same namespace, different labels.
    different_labels_pod = make_kube_pod(kube_api, name='different-labels',
                                         labels={'something': 'different'})
    different_labels_labels = different_labels_pod.metadata.labels
    different_labels_port = different_labels_pod.spec.containers[0].ports[0].container_port
    different_labels_ip = different_labels_pod.status.pod_ip
    different_labels_targets = get_pod_destination(kube_api, 'default', different_labels_labels,
                                                   different_labels_port)
    assert different_labels_targets == {ALBTarget(ip=different_labels_ip,
                                                  http_port=different_labels_port)}

    # Different namespace, same labels.
    another_ns_pod = make_kube_pod(kube_api, namespace='another')
    another_ns_labels = another_ns_pod.metadata.labels
    another_ns_port = another_ns_pod.spec.containers[0].ports[0].container_port
    another_ns_ip = another_ns_pod.status.pod_ip
    another_ns_targets = get_pod_destination(kube_api, 'another', another_ns_labels,
                                             another_ns_port)
    assert another_ns_targets == {ALBTarget(ip=another_ns_ip, http_port=another_ns_port)}

    # By selecting a nonexistent node, this pod will never run and get an IP.
    pod_no_ip = make_kube_pod(
        kube_api,
        name='no-ip',
        labels={
            'app.kubernetes.io/name': 'no-ip-pod',
            'app.kubernetes.io/instance': 'no-ip-pod-a',
        },
        node_selector={'not': 'found'},
        wait=False,
    )
    no_ip_labels = pod_no_ip.metadata.labels
    no_ip_port = pod_no_ip.spec.containers[0].ports[0].container_port
    assert pod_no_ip.status.pod_ip is None
    targets = get_pod_destination(kube_api, 'default', no_ip_labels, no_ip_port)
    assert targets == set()


def test_resolve_service(kube_api, clean_kube):
    """Test pod label targeting in resolve_service."""
    different_labels = {**POD_LABELS, 'app.kubernetes.io/instance': 'ab'}
    service = kube_api.create_namespaced_service(
        namespace='default',
        body={
            'metadata': {'name': 'name'},
            'spec': {'selector': POD_LABELS, 'ports': [PORT_NAMED]},
        }
    )
    different_service = kube_api.create_namespaced_service(
        namespace='default',
        body={
            'metadata': {'name': 'different-service'},
            'spec': {'selector': different_labels, 'ports': [PORT_NAMED]},
        }
    )

    def resolve(service_name=service.metadata.name):
        return resolve_service(
            kube_api,
            namespace='default',
            service_name=service_name,
            service_port=PORT_NAMED['port'],
        )

    assert resolve() == set()

    # Non-matching pods ignored.
    make_kube_pod(kube_api, name='pod-no-labels', labels=None, port=1212)
    pod_different_labels = make_kube_pod(
        kube_api, name='pod-different-labels', labels=different_labels, port=1212)
    assert resolve() == set()
    assert resolve(service_name=different_service.metadata.name) == {
        ALBTarget(ip=pod_different_labels.status.pod_ip,
                  http_port=pod_different_labels.spec.containers[0].ports[0].container_port),
    }

    pod_1 = make_kube_pod(kube_api, name='pod-1', labels=POD_LABELS)
    expected_1 = ALBTarget(ip=pod_1.status.pod_ip,
                           http_port=pod_1.spec.containers[0].ports[0].container_port)
    assert resolve() == {expected_1}

    pod_2 = make_kube_pod(kube_api, name='pod-2', labels=POD_LABELS, port=1212)
    expected_2 = ALBTarget(ip=pod_2.status.pod_ip,
                           http_port=pod_2.spec.containers[0].ports[0].container_port)
    assert expected_1.http_port != expected_2.http_port
    assert resolve() == {expected_1, expected_2}


def test_resolve_service_namespaces(kube_api, clean_kube):
    """Test namespace targeting in resolve_service."""
    kube_api.create_namespace(body={'metadata': {'name': 'another'}})
    service_default = kube_api.create_namespaced_service(
        namespace='default',
        body={
            'metadata': {'name': 'name'},
            'spec': {'selector': POD_LABELS, 'ports': [PORT_NAMED]},
        }
    )
    service_another = kube_api.create_namespaced_service(
        namespace='another',
        body={
            'metadata': {'name': 'name'},
            'spec': {'selector': POD_LABELS, 'ports': [PORT_NAMED]},
        }
    )
    pod_default = make_kube_pod(
        kube_api, namespace='default', name='pod-default-ns', labels=POD_LABELS, port=1212)
    pod_another = make_kube_pod(
        kube_api, namespace='another', name='pod-another-ns', labels=POD_LABELS, port=1212)

    assert resolve_service(
            kube_api,
            namespace='default',
            service_name=service_default.metadata.name,
            service_port=PORT_NAMED['port'],
        ) == {
            ALBTarget(ip=pod_default.status.pod_ip,
                      http_port=pod_default.spec.containers[0].ports[0].container_port)
        }

    assert resolve_service(
            kube_api,
            namespace='another',
            service_name=service_another.metadata.name,
            service_port=PORT_NAMED['port'],
        ) == {
            ALBTarget(ip=pod_another.status.pod_ip,
                      http_port=pod_another.spec.containers[0].ports[0].container_port)
        }


def test_resolve_service_ports(kube_api, clean_kube):
    """Test pod targeting in resolve_service."""
    service_named = kube_api.create_namespaced_service(
        namespace='default',
        body={
            'metadata': {'name': 'named-port'},
            'spec': {'selector': POD_LABELS, 'ports': [PORT_NAMED]},
        }
    )
    service_unnamed = kube_api.create_namespaced_service(
        namespace='default',
        body={
            'metadata': {'name': 'unnamed-port'},
            'spec': {'selector': POD_LABELS, 'ports': [PORT_UNNAMED]},
        }
    )
    service_udp = kube_api.create_namespaced_service(
        namespace='default',
        body={
            'metadata': {'name': 'udp-port'},
            'spec': {'selector': POD_LABELS, 'ports': [PORT_UDP]},
        }
    )

    pod_named_port = make_kube_pod(
        kube_api, namespace='default', name='pod-named-port', labels=POD_LABELS)
    pod_unnamed_port = make_kube_pod(
        kube_api, namespace='default', name='pod-unnamed-port', labels=POD_LABELS,
        ports=[{
            'name': 'tcp-numeric',
            'containerPort': TCP_CONTANER_PORT,
            'protocol': 'TCP',
        }],
    )
    # UDP pod won't be found because we don't target UDP.
    pod_udp_port = make_kube_pod(
        kube_api, namespace='default', name='pod-udp', labels=POD_LABELS,
        ports=[{
            'name': 'udp',
            'containerPort': 212,
            'protocol': 'UDP',
        }],
    )

    assert resolve_service(
        kube_api, namespace='default',
        service_name=service_named.metadata.name,
        service_port=PORT_NAMED['port'],
    ) == {
        ALBTarget(ip=pod_named_port.status.pod_ip,
                  http_port=pod_named_port.spec.containers[0].ports[0].container_port)
    }

    # Target port numbers are taken from the service which matches all pods matching labels.
    # Ports declared by containers are "informational".
    assert isinstance(PORT_UNNAMED['targetPort'], int)
    assert resolve_service(
        kube_api, namespace='default',
        service_name=service_unnamed.metadata.name,
        service_port=PORT_UNNAMED['port'],
    ) == {
        ALBTarget(ip=pod_named_port.status.pod_ip, http_port=PORT_UNNAMED['targetPort']),
        ALBTarget(ip=pod_unnamed_port.status.pod_ip, http_port=PORT_UNNAMED['targetPort']),
        ALBTarget(ip=pod_udp_port.status.pod_ip, http_port=PORT_UNNAMED['targetPort']),
    }

    with pytest.raises(ServiceMissingMappedPort):
        assert resolve_service(
            kube_api, namespace='default',
            service_name=service_udp.metadata.name,
            service_port=PORT_UDP['port'],
        )
