"""High-level testing scenarios."""
from external_alb.kube import load_lb_routing_from_kube
from external_alb.alb import load_lb_config_from_alb
from external_alb.diffing import rules_diff, targets_diff
from external_alb.datamodel import ALBInfo, RuleTargetMap
from external_alb.apply import apply_rules_diff, apply_rule_targets_diff
from external_alb.main import run_controller_step


def test_diff_apply(
    kube_api,
    kube_ev1b1_api,
    kube_resources,
    elbv2,
    alb_resources: ALBInfo,
    sample_actual: RuleTargetMap,
    sample_desired: RuleTargetMap,
):
    """
    Run a scenario involving applying desired ALB configuration.

    This test is quite a mess because it's a pasted script from the "hacking' stage of the project.
    It doesn't actually apply a changed Kubernetes LB routing but a 'desired' sample.
    """
    initial_alb_info = alb_resources

    kube_lb_config = load_lb_routing_from_kube(kube_api, kube_ev1b1_api, ingress_class='merge')
    alb_lb_config, rule_to_arns, alb_info = load_lb_config_from_alb(elbv2, initial_alb_info.name)
    assert kube_lb_config == alb_lb_config == sample_actual
    assert alb_info == initial_alb_info

    rules_changed = rules_diff(kube_lb_config, sample_desired)
    targets_changed = targets_diff(kube_lb_config, sample_desired)

    alb_info_2, created_rule_arns = apply_rules_diff(elbv2, alb_info, rules_changed, rule_to_arns)

    alb_lb_config_2, rule_to_arns_2, alb_info_2_loaded = load_lb_config_from_alb(
        elbv2, initial_alb_info.name)

    assert alb_info_2 == alb_info_2_loaded

    rule_to_arns_expected = {**created_rule_arns, **rule_to_arns}
    for rule in rules_changed.to_remove:
        del rule_to_arns_expected[rule]

    assert rule_to_arns_expected == rule_to_arns_2

    assert rules_changed.to_add.issubset(alb_lb_config_2.keys())
    assert rules_changed.to_remove.issubset(alb_lb_config.keys())
    assert not rules_changed.to_add.issubset(alb_lb_config.keys())
    assert not rules_changed.to_remove.issubset(alb_lb_config_2.keys())

    apply_rule_targets_diff(elbv2, alb_info_2, targets_changed, rule_to_arns_2)

    alb_lb_config_3, rule_to_arns_3, alb_info_3_loaded = load_lb_config_from_alb(
        elbv2, initial_alb_info.name)
    assert alb_lb_config_3 == sample_desired
    assert alb_info_2 == alb_info_3_loaded


def test_controller_iteration(
    kube_api,
    kube_ev1b1_api,
    kube_resources,
    elbv2,
    alb: ALBInfo,
    sample_actual: RuleTargetMap,
):
    """Test applying desired ALB configuration to an empty ALB."""
    (
        pre_alb_lb_config, pre_rule_to_arns, pre_alb_info
    ) = load_lb_config_from_alb(elbv2, alb.name)
    assert pre_alb_lb_config == {}

    run_controller_step(
        kube_api,
        kube_ev1b1_api,
        elbv2,
        alb.name,
        ingress_class='merge',
        tg_name_prefix='alb-',
    )

    (
        post_alb_lb_config, post_rule_to_arns, post_alb_info
    ) = load_lb_config_from_alb(elbv2, alb.name)

    assert post_alb_lb_config == sample_actual
