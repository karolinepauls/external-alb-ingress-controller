#!/usr/bin/env python3
"""External ALB ingress controller - a multi-namespace Kubernetes ingress controller."""
import sys
import subprocess
from pathlib import Path
from setuptools import setup  # type: ignore
from setuptools.command.develop import develop  # type: ignore


class CustomDevelop(develop):
    """Install normal and dev dependencies."""

    def run(self):
        """Set up the local dev environment fully."""
        super().run()
        print('* Installing dev dependencies', file=sys.stderr)
        subprocess.check_call(['pip', 'install', '-U', 'pip'])
        subprocess.check_call(['pip', 'install', '.[dev]'])


VERSION = '0.0.1'
README_FILE = Path(__file__).resolve().with_name('README.rst')
README = README_FILE.read_text('utf-8')
REQUIREMENTS = [
    'boto3==1.13.20',
    'kubernetes==11.0.0',
]
DEV_REQUIREMENTS = [
    'pytest',
    'flake8',
    'pyflakes>=1.6.0',  # Flake8's version doesn't detect types used in string annotations.
    'flake8-docstrings',
    'flake8_tuple',
    'mypy',
    'moto==1.3.14',
]

if __name__ == '__main__':
    setup(
        name='external-alb',
        version=VERSION,
        description='Multi-namespace Kubernetes ALB ingress controller',
        long_description=README,
        classifiers=[
            'Topic :: Utilities',
            'License :: OSI Approved :: BSD License',
            'Programming Language :: Python :: 3.6',
            'Programming Language :: Python :: 3.7',
            'Programming Language :: Python :: 3.8',
        ],
        keywords='kubernetes ingress aws amazon',
        author='Karoline Pauls',
        author_email='code@karolinepauls.com',
        url='https://gitlab.com/karolinepauls/external-alb',
        project_urls={
            "Bug Tracker": 'https://gitlab.com/karolinepauls/external-alb/issues',
            "Source Code": 'https://gitlab.com/karolinepauls/external-alb',
        },
        license='MIT',
        packages=['external_alb'],
        package_data={
            'external_alb': ['py.typed'],
        },
        zip_safe=False,
        install_requires=REQUIREMENTS,
        extras_require={
            'dev': DEV_REQUIREMENTS,
        },
        cmdclass={
            'develop': CustomDevelop,
        },
    )
