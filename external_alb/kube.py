#!/usr/bin/env python3
"""Building "desired" routing state from Kubernetes ingresses, services, and resources."""
import logging
from sys import stderr
from typing import Union, NamedTuple, Dict, Set, DefaultDict
from itertools import chain
from kubernetes import client  # type: ignore
from external_alb.datamodel import ListenerRule, ALBTarget, RuleTargetMap


TIMEOUT_SECONDS = 60

log = logging.getLogger(__name__)


class KubeStateInconsistent(Exception):
    """
    Raised when Kubernetes resources refer to other non-existent resources.

    These errors can be ignored by skipping processing resources with non-existent targets.
    """


class MissingService(KubeStateInconsistent):
    """Raised if a targeted service couldn't be found."""


class ServiceMissingMappedPort(KubeStateInconsistent):
    """Raised if a service does not map the a port mapped by an ingress."""


class KubeService(NamedTuple):
    """Kubernetes service parameters we're interested in."""

    pod_selector: Dict[str, str]
    http_port: Union[int, str]


def dict_to_selector(d: Dict[str, str]) -> str:
    """Build a Kubernetes API 'key=value,key2=value2' selector from a dict."""
    return ','.join(f'{k}={v}' for k, v in d.items())


def query_service(
    api: client.CoreV1Api,
    namespace: str,
    service_name: str,
    service_port: int,
) -> KubeService:
    """Retrieve the pod selector and HTTP port from the service pointed."""
    resp = api.list_service_for_all_namespaces(
        field_selector=f'metadata.name={service_name},metadata.namespace={namespace}'
    ).items
    try:
        [service] = resp
    except ValueError:
        raise MissingService(service_name)
    http_ports = [port.target_port for port in service.spec.ports
                  if port.port == service_port and port.protocol == 'TCP']
    if http_ports == []:
        raise ServiceMissingMappedPort(service_name, service_port)
    [http_port] = http_ports  # Port number or port name.
    return KubeService(
        pod_selector=service.spec.selector if service.spec.selector is not None else {},
        http_port=http_port
    )


def get_pod_destination(
    api: client.CoreV1Api,
    namespace: str,
    pod_labels: Dict[str, str],
    target_port: Union[str, int],
) -> Set[ALBTarget]:
    """Build targets from pods that match the arguments and have an IP address assigned."""
    pods = api.list_pod_for_all_namespaces(
        field_selector=f'metadata.namespace={namespace}',
        label_selector=dict_to_selector(pod_labels)
    )

    targets = set()
    for pod in pods.items:
        ip = pod.status.pod_ip
        # IP of new pods can be None
        if ip is None:
            continue

        if isinstance(target_port, int):
            port_no = target_port
        else:
            # If the port is a string, it must be looked up at the pod.
            pod_ports = chain.from_iterable(container.ports for container in pod.spec.containers)
            http_ports = [port.container_port for port in pod_ports
                          if port.protocol == 'TCP' and port.name == target_port]
            if len(http_ports) == 0:
                log.warning('Pod %s, ns %s missing targeted port: %r',
                            pod.metadata.name, namespace, target_port)
                continue
            # Not supporting duplicate ports in different containers
            # https://stackoverflow.com/questions/58713651/2-containers-using-the-same-port-in-kubernetes-pod
            [port_no] = http_ports

        targets.add(ALBTarget(ip=ip, http_port=port_no))

    return targets


def resolve_service(
    api: client.CoreV1Api,
    namespace: str,
    service_name: str,
    service_port: int,
) -> Set[ALBTarget]:
    """Find pod IP:port targets matched by a service."""
    service = query_service(api, namespace, service_name, service_port)
    alb_targets = get_pod_destination(api, namespace, service.pod_selector, service.http_port)
    return alb_targets


def load_lb_routing_from_kube(
    api: client.CoreV1Api,
    ev1b1_api: client.ExtensionsV1beta1Api,
    ingress_class: str,
) -> RuleTargetMap:
    api_response = ev1b1_api.list_ingress_for_all_namespaces(timeout_seconds=TIMEOUT_SECONDS)

    ingresses = []
    for ingress in api_response.items:
        # TODO: 1.18 define IngressClass and the ingressClassName field on Ingress.
        if ingress.metadata.annotations['kubernetes.io/ingress.class'] == ingress_class:
            ingresses.append(ingress)

    alb_rules: DefaultDict[ListenerRule, Set[ALBTarget]] = DefaultDict(set)
    for ingress in ingresses:
        spec = ingress.spec
        if spec.tls is not None:
            log.warning(
                'IngressTLS ignored. Use the certificates (IAM, ACM) directly with your ALB')
        if spec.backend is not None:
            log.info(
                'Default ingress spec backend ignored, since we\'re merging multiple ingresses.')

        for rule in spec.rules:
            for path in rule.http.paths:
                ingress_rule = ListenerRule(
                    host=rule.host,
                    path=path.path,
                )
                service_name = path.backend.service_name
                try:
                    targets = resolve_service(
                        api,
                        namespace=ingress.metadata.namespace,
                        service_name=service_name,
                        service_port=path.backend.service_port,
                    )
                except KubeStateInconsistent as e:
                    print(f'Error: {e!r} - skipping', file=stderr)
                    continue
                alb_rules[ingress_rule].update(targets)

    frozen_alb_rules = {}
    for rule in alb_rules.keys():
        frozen_alb_rules[rule] = frozenset(alb_rules[rule])
    return frozen_alb_rules
