"""Entry point."""
import logging
from os import environ
from time import sleep
import boto3  # type: ignore
from kubernetes import client, config
from external_alb.kube import load_lb_routing_from_kube
from external_alb.alb import load_lb_config_from_alb
from external_alb.diffing import rules_diff, targets_diff
from external_alb.datamodel import ALBInfo
from external_alb.apply import apply_rules_diff, apply_rule_targets_diff


log = logging.getLogger(__name__)


def run_controller_step(
    kube_api: client.CoreV1Api,
    kube_ev1b1_api: client.ExtensionsV1beta1Api,
    elbv2,
    alb_name: str,
    ingress_class: str,
    tg_name_prefix: str,
) -> ALBInfo:
    """Load LB and Kubernetes LB config and apply if they differ."""
    kube_lb_config = load_lb_routing_from_kube(kube_api, kube_ev1b1_api, ingress_class='merge')
    alb_lb_config, rule_to_arns, alb_info = load_lb_config_from_alb(elbv2, alb_name)

    if kube_lb_config != alb_lb_config:
        log.info('Kubernetes ALB routing changed.')
        rules_changed = rules_diff(alb_lb_config, kube_lb_config)
        targets_changed = targets_diff(alb_lb_config, kube_lb_config)

        updated_alb_info, created_rule_arns = apply_rules_diff(
            elbv2, alb_info, rules_changed, rule_to_arns, tg_name_prefix=tg_name_prefix)
        log.debug('Created ALB rules: %s', created_rule_arns)

        updated_rule_to_arns = {**created_rule_arns, **rule_to_arns}
        apply_rule_targets_diff(elbv2, updated_alb_info, targets_changed, updated_rule_to_arns)

        return updated_alb_info
    return alb_info


def run_controller_loop(
    interval: float,
    alb_name: str,
    tg_name_prefix: str,
    ingress_class: str,
):
    """
    Fetch ALB and Kubernetes LB routing and update ALB config.

    Currently it is the simplest polling loop. No events, no caching of ALB config.
    """
    kube_api = client.CoreV1Api()
    kube_ev1b1_api = client.ExtensionsV1beta1Api(client.ApiClient())
    elbv2 = boto3.client("elbv2")

    while True:
        run_controller_step(
            kube_api,
            kube_ev1b1_api,
            elbv2,
            alb_name,
            ingress_class,
            tg_name_prefix,
        )
        sleep(interval)


def main():
    """Run External ALB Ingress Controller."""
    log_level = environ.get('EXTERNAL_ALB_LOG_LEVEL', 'info')
    logging.basicConfig(level=log_level)

    interval = float(environ.get('EXTERNAL_ALB_INTERVAL', 5))
    alb_name = environ['EXTERNAL_ALB_NAME']
    tg_name_prefix = environ.get('EXTERNAL_ALB_TG_NAME_PREFIX', 'alb-')
    ingress_class = environ['EXTERNAL_ALB_INGRESS_CLASS']

    log.info(
        'External ALB Ingress Controller configuration: \n'
        'polling interval: %s, \n'
        'ALB name: %s, \n'
        'ALB target group name prefix: %s, \n'
        'Kubernetes ingress class: %s, ',
        interval, alb_name, tg_name_prefix, ingress_class
    )

    try:
        config.load_incluster_config()
        log.info("Loaded in-cluster config")
    except config.ConfigException:
        config.load_kube_config()
        log.info("Loaded kubeconfig")

    run_controller_loop(interval, alb_name, tg_name_prefix, ingress_class)
