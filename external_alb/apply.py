"""Applying an ALB setup diff."""
import string
import random
from typing import Tuple
from external_alb.alb import RuleArnsMap, RuleARNs
from external_alb.datamodel import (
    ALBInfo, RulesDiff, TargetDiff, RuleTargetDiff,
)


def make_tg(elbv2, alb_info: ALBInfo, name: str):
    """Create a target group."""
    [target_group] = elbv2.create_target_group(
        Name=name,
        Protocol="HTTP",
        # Targets specify their own ports,
        Port=1,
        VpcId=alb_info.vpc_id,
        HealthCheckProtocol="HTTP",
        # Should be possible to specify healthchecks using an annotation.
        HealthCheckPort="8000",
        HealthCheckPath="/",
        HealthCheckIntervalSeconds=5,
        HealthCheckTimeoutSeconds=5,
        HealthyThresholdCount=5,
        UnhealthyThresholdCount=2,
        Matcher={"HttpCode": "200"},
    )["TargetGroups"]
    return target_group


def apply_rules_diff(
    elbv2,
    alb_info: ALBInfo,
    diff: RulesDiff,
    rule_to_arns: RuleArnsMap,
    tg_name_prefix='alb-'
) -> Tuple[ALBInfo, RuleArnsMap]:
    """
    Create ALB target groups and rules corresponding to the diff.

    Target names will be generated, starting with ``tg_name_prefix``, ending with a random string.

    ALBInfo with updated max_priority and a RuleArnsMap containing ARNs of created resources only
    will be returned.
    """
    max_priority = alb_info.max_priority
    created_rule_arns = {}

    for rule in diff.to_add:
        conditions = []
        if rule.host is not None:
            conditions.append({"Field": "host-header", "Values": [rule.host]})
        if rule.path is not None:
            conditions.append({"Field": "path-pattern", "Values": [rule.path]})

        tg_name = tg_name_prefix + ''.join(
            random.choice(string.ascii_lowercase) for _ in range(20)
        )
        tg_response = make_tg(elbv2, alb_info, tg_name)
        tg_arn = tg_response["TargetGroupArn"]

        max_priority += 1
        rule_response = elbv2.create_rule(
            ListenerArn=alb_info.listener_arn,
            Priority=max_priority,
            Conditions=conditions,
            Actions=[
                # Listener rule must map to something including service name...
                {"TargetGroupArn": tg_arn, "Type": "forward"}
            ],
        )
        rule_arn = rule_response['Rules'][0]['RuleArn']
        created_rule_arns[rule] = RuleARNs(rule_arn=rule_arn, target_group_arn=tg_arn)

    for rule in diff.to_remove:
        elbv2.delete_rule(
            RuleArn=rule_to_arns[rule].rule_arn,
        )
        elbv2.delete_target_group(
            TargetGroupArn=rule_to_arns[rule].target_group_arn,
        )

    return alb_info._replace(max_priority=max_priority), created_rule_arns


def apply_targets_diff(
    elbv2,
    target_group_arn: str,
    target_diff: TargetDiff,
):
    """
    Register/deregister target group's targets.

    Targets are identified by IPs so in case of changing IP1:port1 to IP1:port2, we cannot first
    add IP1:port1 and later remove IP1:port2, because the former will actually change the IP1:port1
    target to IP1:port2, and the latter remove the IP1 target no matter the port.

    We also want to add first and remove later to avoid gaps in case targets are removed and later
    added - so that if anything, race conditions are at least not out fault.
    """
    targets_to_add = [{"Id": target.ip, "Port": target.http_port} for target in target_diff.to_add]
    ips_added = {target.ip for target in target_diff.to_add}
    # Ports don't matter.
    targets_to_remove = [{"Id": target.ip} for target in target_diff.to_remove
                         if target.ip not in ips_added]
    elbv2.register_targets(
        TargetGroupArn=target_group_arn,
        Targets=targets_to_add,
    )
    elbv2.deregister_targets(
        TargetGroupArn=target_group_arn,
        Targets=targets_to_remove,
    )


def apply_rule_targets_diff(
    elbv2,
    alb_info: ALBInfo,
    diff: RuleTargetDiff,
    rule_to_arns: RuleArnsMap,
):
    """Register/deregister targets pointed by the diff."""
    for rule, target_diff in diff.items():
        tg_arn = rule_to_arns[rule].target_group_arn
        apply_targets_diff(elbv2, tg_arn, target_diff)
