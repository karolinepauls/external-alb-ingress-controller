"""Calculating differences between rules and targets in internal model of LB routing."""
from external_alb.datamodel import (
    RuleTargetMap, RulesDiff, TargetDiff, RuleTargetDiff,
)


def rules_diff(actual: RuleTargetMap, desired: RuleTargetMap) -> RulesDiff:
    """Calculate the difference between rules existing in both maps."""
    return RulesDiff(
        to_remove=frozenset(actual.keys() - desired.keys()),
        to_add=frozenset(desired.keys() - actual.keys()),
    )


def targets_diff(actual: RuleTargetMap, desired: RuleTargetMap) -> RuleTargetDiff:
    """Calculate the difference between targets existing in both maps."""
    differences = {}
    for desired_listener, desired_targets in desired.items():
        try:
            actual_targets = actual[desired_listener]
        except KeyError:
            actual_targets = frozenset()
        if desired_targets != actual_targets:
            target_diff = TargetDiff(
                to_add=frozenset(desired_targets - actual_targets),
                to_remove=frozenset(actual_targets - desired_targets),
            )
            differences[desired_listener] = target_diff
    return differences
