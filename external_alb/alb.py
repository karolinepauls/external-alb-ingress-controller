"""Loading ALB state into a common model."""
from typing import Dict, Tuple, NamedTuple
from external_alb.datamodel import ListenerRule, ALBTarget, RuleTargetMap, ALBInfo


class RuleARNs(NamedTuple):
    """ARNs of ALB Rule and Target Group objects related to a listener rule."""

    rule_arn: str
    target_group_arn: str


RuleArnsMap = Dict[ListenerRule, RuleARNs]


def load_lb_config_from_alb(
    elbv2, alb_name: str, listener_port: int = 443
) -> Tuple[RuleTargetMap, RuleArnsMap, ALBInfo]:
    """Fetch ALB rules and targets to build internal representation of its routing."""
    lbs = elbv2.describe_load_balancers(Names=[alb_name])
    [lb] = lbs['LoadBalancers']

    listeners = elbv2.describe_listeners(LoadBalancerArn=lb['LoadBalancerArn'])['Listeners']
    [our_listener] = [listener for listener in listeners
                      if listener['Port'] == listener_port and listener['Protocol'] == 'HTTPS']
    listener_arn = our_listener['ListenerArn']

    rules = elbv2.describe_rules(ListenerArn=listener_arn)['Rules']

    alb_rules = {}
    rule_to_arns = {}
    max_priority = 0
    for rule in rules:
        if rule['IsDefault'] is True:
            continue

        priority = int(rule['Priority'])
        max_priority = max([max_priority, priority])
        rule_arn = rule['RuleArn']

        conditions = rule['Conditions']
        condition_map = {c['Field']: c['Values'] for c in conditions}
        path = condition_map.get('path-pattern')  # not a pattern, lel
        if path is not None:
            [path] = path
        host = condition_map.get('host-header')
        if host is not None:
            [host] = host
        listener_rule = ListenerRule(host=host, path=path)

        try:
            [action] = rule['Actions']
        except ValueError:
            raise Exception(f'Only 1 action supported, rule {rule_arn}')
        if action['Type'] != 'forward':
            print(f'Skipping rule {rule_arn} - not "forward"')

        tg_arn = action['TargetGroupArn']
        health = elbv2.describe_target_health(TargetGroupArn=tg_arn)['TargetHealthDescriptions']

        targets = [h['Target'] for h in health]
        # The targets may not be IPs but instance numbers or lambda ARN. That's OK. They will not
        # have a Kubernetes counterpart and will be removed if Kubernetes takes control of this rule
        # when an ingress controller starts to target this host+path.
        alb_targets = frozenset(ALBTarget(ip=t['Id'], http_port=t['Port']) for t in targets)

        alb_rules[listener_rule] = alb_targets
        rule_to_arns[listener_rule] = RuleARNs(rule_arn, tg_arn)

    alb_info = ALBInfo(
        name=alb_name, vpc_id=lb['VpcId'], listener_arn=listener_arn, max_priority=max_priority,
    )
    return alb_rules, rule_to_arns, alb_info
