"""
Data model expressing Application Load Balancer and Kubernetes routing setups.

The ALB is provided and managed externally, 2 listeners are created for ports 80 and 443 (k8s
ingresses don't support ports other than 80 and 443)

Rules (hosts/paths) get extracted from all matched ingresses.
Each k8s ingress rule → ALB listener rule
K8s Services matching ingress backend → ALB target group
K8s Pod IP (matched by service labels) → ALB target
"""
from typing import NamedTuple, Dict, FrozenSet, Optional


class ALBInfo(NamedTuple):
    """Information about an external ALB, which we configure but not create nor delete."""

    name: str
    vpc_id: str
    listener_arn: str
    max_priority: int


class ListenerRule(NamedTuple):
    """Kubernetes ingress rule/ALB listener rule + target group."""

    host: Optional[str]
    path: Optional[str]


class ALBTarget(NamedTuple):
    """
    Individual targeted pod IP:port pair.

    Maps to a Kubernetes pod/ALB target.
    """

    ip: str
    http_port: int


RuleTargetMap = Dict[ListenerRule, FrozenSet[ALBTarget]]


class RulesDiff(NamedTuple):
    """Difference in listener rules."""

    to_add: FrozenSet[ListenerRule]
    to_remove: FrozenSet[ListenerRule]


class TargetDiff(NamedTuple):
    """Difference in ALBTargets of an individual listener."""

    to_add: FrozenSet[ALBTarget]
    to_remove: FrozenSet[ALBTarget]


RuleTargetDiff = Dict[ListenerRule, TargetDiff]
